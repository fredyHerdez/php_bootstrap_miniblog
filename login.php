<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">    
    <title>Ingresar</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/signin.css" rel="stylesheet">
  </head>
  <body>      
    <div class="container">
        <form action="log.php"  method="post" class="form-signin">
        <h2 class="form-signin-heading">Ingreso al Sistema</h2>        
        <input type="text" name="username" class="form-control" placeholder="Usuario" required autofocus>
        <input type="password" name="password"  class="form-control" placeholder="Contraseña" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
      </form>
    </div>
    <div class="container">
        <form action="registrar.php"  method="post" class="form-signin">
        <h2 class="form-signin-heading">Registrar</h2>        
        <input type="text" name="nombre" class="form-control" placeholder="Nombre" required autofocus>
        <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" required autofocus>
        <input type="text" name="correo" class="form-control" placeholder="Correo" required autofocus>
        <input type="text" name="username" class="form-control" placeholder="Usuario" required autofocus>
        <input type="password" name="password"  class="form-control" placeholder="Contraseña" required>
        <button class="btn btn-lg btn-success btn-block" type="submit">Registrar</button>
      </form>
    </div>
  </body>
</html>
