<?php 
class MySQL{
  private $conexion; public $isConnected;
  public function MySQL(){    
    $this->isConnected = true;
    try
    { 
      $this->conexion = new PDO('mysql:dbname=curso; host 127.0.0.1','root','root');
    } 
    catch(PDOException $e)
    {
      $this->isConnected = false;
      echo $e->getMessage();
    }     
  }
  public function consulta($consulta){
    $statement= $this->conexion->prepare($consulta);
    return $statement;
  }
#------------------------ AGREGAR INFORMACION
  public function addUsuario($nombre,$apellidos,$correo,$username,$password){
    $query="INSERT INTO usuarios (nombre, apellidos, correo, username, password) 
    VALUES ('$nombre', '$apellidos', '$correo', '$username', '$password')";
    echo $query;
    $statement = $this->conexion->prepare($query);
    $statement->execute();    
  }
  public function addPost($username, $titulo, $contenido){
    $query="INSERT INTO post (username, titulo, fecha, contenido) 
    VALUES ('$username', '$titulo','".date("Y-m-d")."','$contenido')";
    echo $query;
    $statement = $this->conexion->prepare($query);
    $statement->execute();    
  }
  public function addComentario($post, $username, $contenido){
    $query="INSERT INTO comentario (post, username, contenido) 
    VALUES ('$post', '$username', '$contenido')";
    echo $query;
    $statement = $this->conexion->prepare($query);
    $statement->execute();    
  }
  public function seguir($username, $usernameAmigo){
    $query="INSERT INTO amigos (username1, username2) 
    VALUES ('$username', '$usernameAmigo')";
    echo $query;
    $statement = $this->conexion->prepare($query);
    $statement->execute();    
  }
#------------------------- OBTENER INFORMACION
  public function getUser($username){
    $statement= $this->consulta("SELECT * FROM usuarios WHERE username = '$username'" );        
    if($statement->execute()){
      while($fila = $statement->fetch()){
      $usuario = array('nombre' => $fila['nombre'], 'apellidos' => $fila['apellidos'], 'correo' => $fila['correo'], 'username' => $fila['username']);        
      }
    }
    return $usuario;
  }

  public function getUserPost($post){
    $statement= $this->consulta("SELECT username FROM post WHERE id = '$post'" );        
    if($statement->execute()){
      while($fila = $statement->fetch()){
      $usuario = $fila['username'];        
      }
    }
    return $usuario;
  }
  public function getPost($username){
    $statement= $this->consulta("SELECT * FROM post WHERE username = '$username' ORDER BY id DESC" );
    $post="";
    if($statement->execute()){
      while($fila = $statement->fetch()){
        $post .= '<div class="panel panel-default">
            <div class="panel-heading">
              <p class="text-center">
                <label class="text-info"><a href="verPost.php?id='.$fila['id'].'"><h3>'.$fila['titulo'].'</h3></a></label>
              </p>
            </div>
            <div class="panel-body">
              <div class="col-sm-4">
                <a href="verPost.php" class="thumbnail">
                  <img src="img/curso.jpg" />
                </a>
              </div>
              <div class="col-sm-8">
                <h5 class="text-muted">Fecha de publicacion:'.$fila['fecha'].'</h5>
                <h4>
                  '.$fila['contenido'].'
                </h4>
                <br><br>
              </div>
            </div>
          </div>';;
      }
    }
    return $post;
  }
  public function getPostEspecifico($id){
    $statement= $this->consulta("SELECT * FROM post WHERE id = '$id'ORDER BY id DESC" );
    $post="";
    if($statement->execute()){
      while($fila = $statement->fetch()){
        $post .= '
        <div class="panel ">
          <div class="row panel-heading">
            <h1 class="text-primary">'.$fila['titulo'].'</h1>
          </div>
          <div class="panel-body">
            <h3 class="text-muted"><small>POSTED BY @'.$fila['username'].'  '.$fila['fecha'].'--  '.$this->getTotalComentario($id).' COMMENTS</small></h3>
            <p class="text-justify">
            '.$fila['contenido'].'              
          </p>
          </div>                    
        </div>';
      }
    }
    return $post;
  }
  public function getMejoresPost(){
    $statement= $this->consulta("SELECT * FROM post ORDER BY id DESC LIMIT 0, 5" );
    $post="";
    if($statement->execute()){
      while($fila = $statement->fetch()){
        $post .= '
        <blockquote>      
          <a href="verPost.php?id='.$fila['id'].'>
            <p class="text-primary">'.$fila['titulo'].'</p>
            <p class="text-muted"><small> "@'.$fila['username'].' - '.$fila['fecha'].'</small><p>
          </a>
        </blockquote>
        ';
      }
    }
    return $post;
  }
  public function getPosts($username){
    $statement= $this->consulta("SELECT username2 FROM amigos WHERE username1 = '$username' UNION SELECT username1 FROM amigos WHERE username2 = '$username' LIMIT 0,20");
    $users= "username = '$username'";
    if($statement->execute()){
      while($fila = $statement->fetch()){
        $users .= " || username = '".$fila['username2']."'";
      }
    }  
    $statement= $this->consulta("SELECT * FROM post WHERE $users ORDER BY id DESC" );
    $post="";
    if($statement->execute()){
      while($fila = $statement->fetch()){
        $post .= '<div class="panel panel-default">
            <div class="panel-heading">
              <p class="text-center">
                <label class="text-info"><a href="verPost.php?id='.$fila['id'].'"><h3>@'.$fila['username'].': '.$fila['titulo'].'</h3></a></label>
              </p>
            </div>
            <div class="panel-body">
              <div class="col-sm-4">
                <a href="verPost.php" class="thumbnail">
                  <img src="img/curso.jpg" />
                </a>
              </div>
              <div class="col-sm-8">
                <h5 class="text-muted">Fecha de publicacion:'.$fila['fecha'].'</h5>
                <h4>
                  '.$fila['contenido'].'
                </h4>
                <br><br>
              </div>
            </div>
          </div>';
      }
    }
    return $post;
  }
  public function getComentarios($post){
    $statement= $this->consulta("SELECT * FROM comentario WHERE post = '$post'");
    $comentario="";
    if($statement->execute()){
      while($fila = $statement->fetch()){
        $comentario .= '
          <div class="row">
            <div class="col-sm-2">
              <img src="img/usuario.jpg" alt="Responsive-img" class="img-responsive img-circle" style="height:40px"/>
            </div>
            <div class="col-sm-10">
              <a href="perfil.php?id='.$fila['username'].'"><h4>@'.$fila['username'].'</h4></a>
              '.$fila['contenido'].'
            </div>
          </div>
        ';
      }
    }
    return $comentario;
  }
  public function getSigoA($username){
    $statement= $this->consulta("SELECT * FROM amigos WHERE username1 = '$username'" );
    $sigoa="";
    if($statement->execute()){
      while($fila = $statement->fetch()){
        $sigoa .= '
            <div class="panel-body">
              <div class="col-sm-4">
                <img  class="img-responsive img-circle" src="img/usuario.jpg"
                alt="Responsive-img" />
              </div>
              <div class="col-sm-8">
                <a href="#"><h4>@'.$fila['username2'].'</h4></a>
              </div>
            </div>
        ';
      }
    }
    return $sigoa;
  }
  public function getSolicitudes($username){
    $statement= $this->consulta("SELECT * FROM amigos WHERE username2 = '$username' && username1 NOT IN (SELECT username2 FROM amigos WHERE username1 = '$username')" );
    $solicitud="";
    if($statement->execute()){
      while($fila = $statement->fetch()){
        $solicitud .= '
            <blockquote >
              <div class="row">
                <div class="col-xs-4">
                  <img src="img/usuario.jpg"
                  alt="Responsive-img" class="img-responsive img-circle" />
                </div>
                <div class="col-sm-8">
                  <a href="perfil.php?id="><p>@'.$fila['username1'].'</p></a>
                    <a href="seguir.php?username='.$fila['username1'].'" class="btn btn-primary btn-xs">Seguir</a>
                </div>
              </div>
              <br>
            </blockquote>
        ';
      }
    }
    return $solicitud;
  }
  public function getTotalPost($username){
    $statement= $this->consulta("SELECT COUNT(*) FROM post WHERE username = '$username'" );    
    $total = "0";
    if($statement->execute()){
      while($fila = $statement->fetch()){ 
        $total=$fila['COUNT(*)'];
      }
    }
    return $total;
  }
  public function getTotalComentario($post){
    $statement= $this->consulta("SELECT COUNT(*) FROM comentario WHERE post = '$post'" );    
    $total = "0";
    if($statement->execute()){
      while($fila = $statement->fetch()){ 
        $total=$fila['COUNT(*)'];
      }
    }
    return $total;
  }
  public function buscar($username){
    $statement= $this->consulta("SELECT * FROM usuarios WHERE username LIKE '%$username%'");
    $usuario="";
    if($statement->execute()){
      while($fila = $statement->fetch()){
        $usuario .= '
            <div class="panel panel-primary">
              <div class="panel-body">
                <div class="col-sm-4">
                  <img  class="img-responsive img-circle" src="img/usuario.jpg"
                  alt="Responsive-img" />
                </div>
                <div class="col-sm-8">
                  <a href="perfil.php?id='.$fila['username'].'"><h4>@'.$fila['username'].'</h4></a>
                  <h5 class="text-muted">'.$fila['nombre'].' '.$fila['apellidos'].'</h5>
                  <h5>Contacto: '.$fila['correo'].'</h5>
                  <div class="row">
                    <div class="col-sm-4">
                      <p class="text-primary">'.$this->getTotalPost($fila['username']).' posts<p>
                    </div>
                    <div class="col-sm-4 col-sm-offset-4">
                      <a href="seguir.php?username='.$fila['username'].'" class="btn btn-primary btn-xs">Seguir</a>
                      <!-- <a href="#" class="btn btn-danger btn-xs">Dejar de seguir</a>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
        ';
      }
    }
    return $usuario;
  }

#------------------------------------------------ ACTUALIZAR INFO

  public function updateAmigo($username, $username2){    
    $query="UPDATE amigos SET enlace=1 WHERE username1 = '$username' && username2 = '$username2'";
    echo $query;
    $statement = $this->conexion->prepare($query);
    $statement->execute();
  }
}
        
?>
