<?php
session_start();
include 'recursos.php';
include 'SQL.php';
$conn = new MySQL();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/index.css">
    <title>Blog del curso</title>
    <style media="screen">
      .sidePanelCols {
        margin: 2px;
        padding: 2px;
      }
    </style>
  </head>

  <?php
  $q;
  if(isset($_GET['q'])){
    $q = $_GET['q'];
  }else{
    $q="";
  }
  ?>
  <body>

    <!-- Barra de navegacion -->
    <!-- Barra de navegacion -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">
            <img src="img/php.png" alt="Brand"  style="height:40px" />
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php" >Incio</a></li>
            <li><a href="perfil.php?id=<?php echo $_SESSION['user'];?>" >Perfil<div></a></li>
            <li><a href="nuevoPost.php" >Nuevo</a></li>
            <li><a href="unlog.php" class="last">Cerrar sesión</a></li>
          </ul>
          <form class="navbar-form navbar-right" action="searchUser.php" method="get">
            <input type="text" name="s" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <br><br><br><br>
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
          <form class="form" action="searchUser.php" method="get">
            <input class="form-control" type="text" name="q" value="<?php echo $q; ?>" placeholder="Buscar personas....">
          </form>

          <h2 class="text-center text-muted">Resultados de la búsqueda</h2>
          <div class="panel">
           <?php echo $conn->buscar($_GET['s']);?>
          </div>
        </div>
      </div>
    </div>


   <!-- NEcesario para bootstrap-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
